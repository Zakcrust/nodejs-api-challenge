# Node JS API Challenge

## Deployment (Docker)

- run build.sh to build the app image
- run run.sh start container of the app image
- run test.sh to start the test case


API URL of the test case script might not be accurate you can use this command to retrieve the correct IP address of the app container
```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nodejs-api-challenge
```
