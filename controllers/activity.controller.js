const Activity = require('../models/index')['activity']
const { body, validationResult, param } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('email', 'Invalid email').exists().isEmail(),
                body('title', `title must not be empty`).exists()
            ]
        }
        case 'update': {
            return [
                param('id', 'Invalid id').exists().isNumeric(),
                body('email', 'Email must not be empty').exists(),
                body('email', 'Invalid email format').isEmail(),
                body('title', `title must not be empty`).notEmpty()
            ]
        }
        case 'delete': {
            return [
                param('id', 'Invalid id').exists().isNumeric()
            ]
        }
    }
}

exports.get = async (req, res, next) => {
    const data = await Activity.findAll();
    return res.status(200).send(
        {
            status: "Success",
            message: "Success",
            data: data
        }
    )
}

exports.getOne = async (req, res, next) => {
    try {
        const data = await Activity.findOne({
            where: {
                id: req.params.id
            }
        })
        if (data) {
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
        else {
            return res.status(404).send({
                status: "Not Found",
                message: `Activity with ID ${req.params.id} Not Found`,
                data: {}
            })
        }
    } catch (error) {
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }
}

exports.create = async (req, res, next) => {
    // const errors = validationResult(req);
    // if (!errors.isEmpty()) {
    //   return res.status(400).json({ errors: errors.array() });
    // }
    try {
        if (req.body.title == undefined || req.body.title == null) {
            return res.status(400).send({
                status: "Bad Request",
                message: "title cannot be null",
                data: {}
            })
        }
        const data = await Activity.create(req.body)
        if (data) {
            return res.status(201).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
    } catch (error) {
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }

}

exports.update = async (req, res, next) => {
    try {
        console.log(`id ${req.params.id}`);
        if (req.body.title == undefined || req.body.title == null) {
            return res.status(400).send({
                status: "Bad Request",
                message: "title cannot be null",
                data: {}
            })
        }
        const checkId = await Activity.findOne({
            where: {
                id: req.params.id
            }
        })
        if (checkId) {
            await Activity.update(req.body, {
                where: {
                    id: req.params.id
                }
            })
            const data = await Activity.findOne({
                where: {
                    id: req.params.id
                }
            })
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
        else {
            return res.status(404).send({
                status: "Not Found",
                message: `Activity with ID ${req.params.id} Not Found`,
                data: {}
            })
        }
    } catch (error) {
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }

}

exports.delete = async (req, res, next) => {
    try {
        const data = await Activity.destroy({
            where: {
                id: req.params.id
            }
        })
        if (data) {
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: {}
            })
        }
        else {
            return res.status(404).send({
                status: "Not Found",
                message: `Activity with ID ${req.params.id} Not Found`,
                data: {}
            })
        }
    } catch (error) {
        return res.status(404).send({
            status: "Not Found",
            message: `Activity with ID ${req.params.id} Not Found`,
            data: {}
        })
    }

}