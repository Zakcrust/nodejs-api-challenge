const ToDo = require('../models/index')['todo'];
const Activity = require('../models/index')['activity'];
const { body, validationResult, param, check } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('activity_group_id', 'activity_group_id must not be empty').exists(),
                body('activity_group_id', 'invalid activity_group_id value').isNumeric(),
                body('priority', 'priority must not be empty').exists(),
                body('priority', 'Invalid priority value').isIn(['Very High', 'High', 'Medium', 'Low', 'Very Low']),
                body('title', `title must not be empty`).exists()
            ]
        }
        case 'update': {
            return [
                body('activity_group_id', 'invalid activity_group_id value').isNumeric(),
                body('activity_group_id', 'activity_group_id must not be empty').exists(),
                param('id', 'Invalid id').exists().isNumeric(),
                body('priority', 'priority must not be empty').exists(),
                body('priority', 'Invalid priority value').isIn(['Very High', 'High', 'Medium', 'Low', 'Very Low']),
                body('is_active', 'is_active must not be empty').exists(),
                body('is_active', 'Invalid value type').isBoolean(),
                body('title', `title must not be empty`).exists()
            ]
        }
        case 'delete': {
            return [
                param('id', 'Invalid id').isNumeric()
            ]
        }
    }
}

exports.get = async (req, res, next) => {
    var data;
    if (req.query.activity_group_id != undefined && !isNaN(req.query.activity_group_id)) {
        data = await ToDo.findAll({
            where: {
                activity_group_id: req.query.activity_group_id
            }
        })
    }
    else {
        data = await ToDo.findAll();
    }
    return res.status(200).send(
        {
            status: "Success",
            message: "Success",
            data: data
        }
    )
}

exports.getOne = async (req, res, next) => {
    try {
        const data = await ToDo.findOne({
            where: {
                id: req.params.id
            }
        })
        if (data) {
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
        else {
            return res.status(404).send({
                status: "Not Found",
                message: `Todo with ID ${req.params.id} Not Found`,
                data: {}
            })
        }
    } catch (error) {
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }
    
}

exports.create = async (req, res, next) => {
    try {
        if (req.body.title == undefined || req.body.title == null) {
            return res.status(400).send({
                status: "Bad Request",
                message: "title cannot be null",
                data: {}
            })
        }
        if (req.body.activity_group_id == undefined || req.body.activity_group_id == null) {
            return res.status(400).send({
                status: "Bad Request",
                message: "activity_group_id cannot be null",
                data: {}
            })
        }
        console.log(`body request : ${JSON.stringify(req.body)}`)
        req.body.is_active = true;
        if(req.body.priority == undefined) req.body.priority = 'very-high';
        const data = await ToDo.create(req.body)
        if (data) {
            return res.status(201).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
    } catch (error) {
        console.log(`Unexpected error : ${error.message}`)
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }
    
}

exports.update = async (req, res, next) => {
    try {
        const checkId = await ToDo.findOne({
            where: {
                id: req.params.id
            }
        })
        if(!checkId)
        {
            return res.status(404).send({
                status: "Not Found",
                message: `Todo with ID ${req.params.id} Not Found`,
                data: {}
            })
        }
        if(req.body.activity_group_id != undefined && req.body.activity_group_id != null)
        {
            const checkActivity = await Activity.findOne({
                where: {
                    id: req.body.activity_group_id
                }
            })
            if (checkActivity) {
                console.log(`id ${req.params.id}`);
                await ToDo.update(req.body, {
                    where: {
                        id: req.params.id
                    }
                })
                const data = await ToDo.findOne({
                    where: {
                        id: req.params.id
                    }
                })
                return res.status(200).send({
                    status: "Success",
                    message: "Success",
                    data: data
                })
            }
            else {
                return res.status(404).send({
                    status: "Not Found",
                    message: `Activity with Activity Group ID ${req.body.activity_group_id} Not Found`,
                    data: {}
                })
            }
        }
        else {
            await ToDo.update(req.body, {
                where: {
                    id: req.params.id
                }
            })
            const data = await ToDo.findOne({
                where: {
                    id: req.params.id
                }
            })
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: data
            })
        }
        
        
    } catch (error) {
        console.log(error.message || error)
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }

}

exports.delete = async (req, res, next) => {
    try {
        const data = await ToDo.destroy({
            where: {
                id: req.params.id
            }
        })
        if (data) {
            return res.status(200).send({
                status: "Success",
                message: "Success",
                data: {}
            })
        }
        else {
            return res.status(404).send({
                status: "Not Found",
                message: `Todo with ID ${req.params.id} Not Found`,
                data: {}
            })
        }   
    } catch (error) {
        return res.status(500).send({
            status: "Unexpected Error",
            message: error.message,
            data: {}
        })
    }
}