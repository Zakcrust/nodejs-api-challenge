/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema
  .createTable('activities', (table) => {
    table.increments('id');
    table.string('email', 255);
    table.string('title', 255);
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  })
  .createTable('todo', (table) => {
    table.increments('id');
    table.string('title', 255);
    table.boolean('is_active');
    table.string('priority', 255);
    table.integer('activity_group_id');
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
    .dropTable("activities")
    .dropTable("todos");
};
