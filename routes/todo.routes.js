var express = require('express');
var router = express.Router();
const controller = require('../controllers/todo.controller')

router.get('/', [], controller.get);
router.post('/', [], controller.create);
router.get('/:id', [], controller.getOne);
router.patch('/:id', [], controller.update);
router.delete('/:id', [], controller.delete);

module.exports = router;
